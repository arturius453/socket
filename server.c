#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#define PORT 8080

int main(){
		int sockfd=socket(AF_INET,SOCK_STREAM,0);
		struct sockaddr_in address;
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = INADDR_ANY;
		address.sin_port = htons( PORT );
		bind(sockfd, (struct sockaddr *)&address,sizeof(address));
		listen(sockfd, 1);
		int addrlen = sizeof(address);
		int new_socket=-1;
		while(
		( new_socket= accept(sockfd, (struct sockaddr *)&address, (socklen_t*)&addrlen))
		>-1)
		{
		int b;
		int valread = read( new_socket , &b, sizeof(int));
		b*=2;
		send(new_socket , &b, sizeof(int) , 0 );
		}
		return 0;
}
