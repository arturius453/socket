#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#define PORT 8080

int main(int argc, char *argv[])
{
    int sock=socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    char* adress=(argc<2) ? "127.0.0.1" : argv[1]; 
    if(inet_pton(AF_INET,adress , &serv_addr.sin_addr)<=0) {
        perror("Invalid address/ Address not supported \n");
        return -1;
    }
    if(connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr))< 0) {
        perror("Error : Connect Failed \n");
        return 2;
    }

	int a;
	scanf("%d",&a);
    send(sock , &a , sizeof(int) , 0 );
    read(sock , &a, sizeof(int));
    printf("%d\n",a );
    return 0;
}
